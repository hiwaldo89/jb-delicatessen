// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const axios = require("axios");
module.exports = function(api) {
  api.loadSource(async ({ addCollection }) => {
    // Use the Data Store API here: https://gridsome.org/docs/data-store-api/
    const baseUrl = "https://jb.somosbloom.com/wp-json/wp/v2";

    // WordPressPage Collection
    const wordPressPagesData = await axios.get(`${baseUrl}/pages`);
    const WordPressPagesCollection = addCollection("WordPressPage");

    for (const item of wordPressPagesData.data) {
      WordPressPagesCollection.addNode({
        id: item.id,
        title: item.title.rendered,
        slug: item.slug,
        acf: item.acf
      });
    }

    // WordPressProductCategory Collection
    const wordPressProductCategoriesData = await axios.get(
      `${baseUrl}/product_category`
    );
    const WordPressProductCategoryCollection = addCollection(
      "WordPressProductCategory"
    );
    for (const item of wordPressProductCategoriesData.data) {
      WordPressProductCategoryCollection.addNode({
        id: item.id,
        title: item.name,
        slug: item.slug,
        acf: item.acf
      });
    }

    // WordPressProduct Collection
    const wordPressProductData = await axios.get(`${baseUrl}/producto`);
    const WordPressProductCollection = addCollection("WordPressProduct");
    for (const item of wordPressProductData.data) {
      WordPressProductCollection.addNode({
        id: item.id,
        title: item.title.rendered,
        product_category: item.product_category,
        acf: item.acf,
        maridajes: item.maridaje
      });
    }

    // WordPressMaridajesCategory Collection
    const wordPressMaridajeCategoryData = await axios.get(
      `${baseUrl}/maridaje`
    );
    const WordPressMaridajeCategoryCollection = addCollection(
      "WordPressMaridajeCategory"
    );
    for (const item of wordPressMaridajeCategoryData.data) {
      WordPressMaridajeCategoryCollection.addNode({
        id: item.id,
        title: item.name,
        slug: item.slug,
        acf: item.acf
      });
    }

    // WordPressSalePoint Collection
    const wordPressSalePointData = await axios.get(`${baseUrl}/punto_de_venta`);
    const WordPressSalePointCollection = addCollection("WordPressSalePoint");
    for (const item of wordPressSalePointData.data) {
      WordPressSalePointCollection.addNode({
        id: item.id,
        title: item.title.rendered,
        acf: item.acf
      });
    }
  });

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  });
};
