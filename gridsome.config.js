// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const path = require("path");

function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/assets/sass/*.scss")]
    });
}

module.exports = {
  siteName: "Gridsome",
  plugins: [
    // {
    //   use: "@gridsome/source-wordpress",
    //   options: {
    //     baseUrl: "https://jb.somosbloom.com", // required
    //     apiBase: "wp-json",
    //     typeName: "WordPress",
    //     perPage: 100,
    //     concurrent: 10,
    //     customEndpoints: [
    //       {
    //         typeName: "AcfProductCategory",
    //         route: "acf/v3/product_category"
    //       }
    //     ]
    //   }
    // },
    {
      use: "gridsome-plugin-tailwindcss",
      options: {
        //tailwindConfig: "./some/file/js",
        purgeConfig: {
          whitelistPatternsChildren: [/aos/]
        },
        presetEnvConfig: {},
        shouldPurge: true,
        shouldImport: true,
        shouldTimeTravel: true,
        shouldPurgeUnusedKeyframes: true
      }
    }
  ],
  // templates: {
  //   WordPressProductCategory: "/productos/categoria/:slug"
  // },
  chainWebpack(config) {
    // Load variables for all vue-files
    const types = ["vue-modules", "vue", "normal-modules", "normal"];

    // or if you use scss
    types.forEach(type => {
      addStyleResource(config.module.rule("scss").oneOf(type));
    });
  }
};
